// Imports: Dependencies
const path = require('path');

console.log("path = " + path.resolve(__dirname, "./src"));

require("babel-register");
// Webpack Configuration
const config = {
  // Entry
  entry: "./src/main.js",
  // Output
  output: {
    path: path.resolve(__dirname, "./public"),
    filename: "main-bundle.js",
  },
  mode: "development",
  // Loaders
  module: {
    rules : [
      // JavaScript/JSX Files
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      // CSS Files
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      }
    ]
  },
  // Plugins
  plugins: [],
};
// Exports
module.exports = config;
