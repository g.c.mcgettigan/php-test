import React  from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router } from "react-router-dom";
import App from "./app.js";

ReactDOM.render(
  <Router  basename="/Fast/Pokedex/public/">
    <App />
  </Router>,
  document.getElementById('react-root')
);
