import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import pokedexApi from '../api/pokedexApi.js'

class SearchView extends Component {
  constructor(props) {
    super(props);

    var itemsPerPage = 50;
    this.state = {
      pokemonCount: props.pokemonCount, //total count of pokemon in list
      pokemon: props.pokemon, // list of all pokemon returned from api
      pokemonList: props.pokemon, // list ready to show on screen, may be filtered from full list
      pageIndex: 0, // current pagination pageIndex
      pageCount: Math.ceil(props.pokemonCount / itemsPerPage), // number of pagination pages
      itemsPerPage: itemsPerPage, // current number of items shown per page

      searchTerm: '', // serach term to filter list
      updateList: true // should the list be re-filtered?
    }

    this.searchChanged = this.searchChanged.bind(this); //handler for search box noChange event
    this.goNext = this.goNext.bind(this);
    this.goPrev = this.goPrev.bind(this);
    this.selectItemsPerPage = this.selectItemsPerPage.bind(this);
  }

  //update list of pokemon and pokemonCount
  static getDerivedStateFromProps(props, state) {
    var newState = {};

    if(props.pokemon !== undefined) {

      newState.pokemon = props.pokemon;

      // only copy pokemon to pokemonList on first time, do not overwrite filtering of pokemonList
      if(state.pokemonList.length == 0) {
        newState.pokemonList = props.pokemon;
      }

    }

    if(props.pokemonCount !== undefined &&
      typeof(props.pokemonCount) === 'number') {

      if(state.pokemonList.length === 0) {
        newState.pokemonCount = props.pokemonCount;
      }
      newState.pageCount = Math.ceil(props.pokemonCount / state.itemsPerPage)
    }

    return newState;
  }

  //update list if the flag has been set, and reset updateList flag
  componentDidUpdate(prevProps, prevState) {
    if(this.state.updateList) {
      var newState = {};

      // update pageCount calculation
      if(this.props.pokemonCount !== undefined &&
        typeof(this.props.pokemonCount) === 'number') {

        newState.pageCount = Math.ceil(this.props.pokemonCount / this.state.itemsPerPage)
      }

      //re-filter the filst
      this.filterPokemon();

      // reset the updateList flag
      newState.updateList = false;

      this.setState(newState);
    }

    return null;
  }

  //onChange Handler when search value is changed
  searchChanged(event) {
    this.setState({
      searchTerm: event.target.value,
      updateList: true
    });

  }

  //goto prev pagination page
  goPrev(event) {
    var newIndex = this.state.pageIndex - 1;
    if(newIndex < 0) {
      newIndex = 0;
    }

    this.setState({
      pageIndex: newIndex
    });
  }
  //goto next pagination page
  goNext(event) {
    var newIndex = this.state.pageIndex + 1;
    if(newIndex > this.state.pageCount) {
      newIndex = this.state.pageCount;
    }

    this.setState({
      pageIndex: newIndex
    });
  }
  //go to specific pagination page
  goToPage(i, event) {

    this.setState({
      pageIndex: i
    });
  }

  selectItemsPerPage(event) {
    var itemsPerPage = parseInt(event.target.value);

    this.setState({
      itemsPerPage: itemsPerPage,
      updateList: true
    });
  }

  // filters the state's pokemonList based on the searchTerm
  filterPokemon() {
    let searchTerm = this.state.searchTerm.trim();
    let alwaysMatch = false;

    if(searchTerm.length < 1) {
      alwaysMatch = true;
    }

    //filter by search term
    var filteredPokemon = new Array();

    for(var pokemon of this.state.pokemon) {

      //give value to the match
      var searchValue = 0;

      if(pokemon.name === searchTerm || alwaysMatch) {
        searchValue += 1000;
      } else if(pokemon.name.indexOf(searchTerm) === 0) {
        searchValue += 500;
      } else if(pokemon.name.indexOf(searchTerm) > 0) {
        searchValue += 100;
      }

      // if has any match then add the the list
      if(searchValue > 0) {
        filteredPokemon.push({
          name: pokemon.name,
          url: pokemon.url,
          searchValue: searchValue
        });
      }
    }

    //sort the list by searchValue and calculate the pageCount
    var pokemonList = filteredPokemon.sort((a, b) => { return b.searchValue - a.searchValue; }  );
    var pageCount = Math.ceil(filteredPokemon.length / this.state.itemsPerPage);

    //update
    var newState = {
      pokemonCount: filteredPokemon.length,
      pokemonList: pokemonList,
      pageCount: pageCount,
      pageIndex: 0
    };

    this.setState(newState);

  }

  // get the renderable list items for the pokemon list
  getRenderListOfPokemon() {
    var list = [];

    // loop over the pokemonList and add the list items to the list
    if(this.state.pokemonList && this.state.pokemonList.length > 0) {

      var startIndex = (this.state.pageIndex * this.state.itemsPerPage);
      var endIndex = startIndex + this.state.itemsPerPage;

      for(var i = startIndex; i<endIndex && i < this.state.pokemonList.length; ++i) {

        var pokemon = this.state.pokemonList[i];

        list.push(
          <li key={pokemon.name} className='search-list-item'>
            <Link to={'/pokemon/' + pokemon.name }>
              { pokemon.name }
            </Link>
          </li>
        );

      }
    }

    return list;
  }

  getRenderPageLinks() {
    let startPageIndex = this.state.pageIndex - 4;
    if(startPageIndex < 0) {
      startPageIndex = 0;
    }
    let startPokemonIndex = startPageIndex * this.state.itemsPerPage;
    console.log("startPageIndex = " + startPageIndex + " startPokemonIndex = " + startPokemonIndex);

    let endPageIndex = startPageIndex + 9;
    //let startPokemonIndex = startPageIndex * this.state.itemsPerPage;
    let endPokemonIndex = endPageIndex * this.state.itemsPerPage;

    console.log("endPokemonIndex = " + endPokemonIndex);
    if(endPokemonIndex > this.state.pokemonCount) {
      endPageIndex = Math.ceil(this.state.pokemonCount / this.state.itemsPerPage);
      console.log("endPageIndex set to " + endPageIndex);
    }

    var pageLinks = [];

    for(var i = startPageIndex; i<endPageIndex; ++i) {

      if(i === this.state.pageIndex) {
        pageLinks.push(<li key={'page-link-' + i} className='page-link-list-item page-link-list-item-current'>{i}</li>);
      } else {
        pageLinks.push(<li key={'page-link-' + i} className='page-link-list-item' onClick={ this.goToPage.bind(this, i) }>{i}</li>);
      }
    }

    var ret = (
      <ul id='page-link-list'>
        { pageLinks }
      </ul>
    );

    return ret;
  }

  render() {

    return(
      <div>
        <div id='search-heading'>
          <div id='search-heading-top'>
            <h2> Search Pokedex </h2>
          </div>
          <div id='search-heading-search-controls'>
            <label htmlFor='search-box'>Search:</label>
            <input type='text' name='search-box' id='search-box' onChange={this.searchChanged} value={ (this.state.searchTerm.trim().length > 0) ? this.state.searchTerm : '' } />
          </div>
        </div>
        <div id='search-list'>
          { this.state.pokemonCount > 0 &&
            <div>
              <ul id='search-list-items'>
                { this.getRenderListOfPokemon() }
              </ul>
              <div id='search-list-pagination-controls'>
                <div id='search-list-pagination-controls-nav'>
                  { this.state.pageIndex > 0 &&
                    <button id='search-list-pagination-controls-prev' onClick={this.goPrev}>
                      {'<'}
                    </button>
                  }
                  {
                    this.getRenderPageLinks()
                  }
                  {
                    this.state.pageIndex < this.state.pageCount &&
                    <button id='search-list-pagination-controls-next' onClick={this.goNext}>
                      {'>'}
                    </button>
                  }
                </div>
                <div>
                  <p>items per page:</p>
                  <select id='search-list-pagination-items-to-show' onChange={this.selectItemsPerPage}>
                    <option value="50">50</option>
                    <option value="100">100</option>
                    <option value="200">200</option>
                  </select>
                </div>
              </div>
            </div>
          }
          { this.state.pokemonCount === 0 &&
            <p id='list-loading'>
              No pokemon to show
            </p>
          }
        </div>
      </div>
    );
  }
}

export default SearchView;
