import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import pokedexApi from '../api/pokedexApi.js';

class PokemonView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pokemon: { nodata: true }
    }
  }

  componentDidMount() {
    pokedexApi.getPokemon(this.props.match.params.name)
      .then((result) => {

        var newState = {
          pokemon: result
        }

        this.setState(newState);
    });
  }

  getRenderPokemonMoves() {
    var movesList = "";
    for(var item of this.state.pokemon.moves) {
      if(movesList != "") {
        movesList += ", ";
      }
      movesList += item.move.name;
    }

    return <p className="col-xs-8 col-sm-8 col-md-9 col-lg-10"> {movesList} </p>
  }

  getRenderPokemonTypes() {
    var types = [];
    for(var item of this.state.pokemon.types) {
      types.push(
        <div className={'type-' + item.type.name }>
          {item.type.name}
        </div>
      );
    }

    return(
      <div id='pokemon-type-list' className="col-xs-8 col-sm-8 col-md-9 col-lg-10">
        { types }
      </div>
    );
  }

  render() {
    return(
      <div id='pokemon'>
        <div id='pokemon-header'>
          <Link id='pokemon-header-back-btn' to='/search'>
            {'< Back'}
          </Link>
        </div>
        <div id='pokemon-content'>
          { this.state.pokemon.nodata &&
            <div id='pokemon-no-data'>
              <h2> No pokemon found </h2>
            </div>
          }
          { !this.state.pokemon.nodata &&
            <div id='pokemon-data'>
              <div className='row'>
                <div id='pokemon-name' className='col-xs-12 col-sm-6 col-md-5 col-lg-4'>
                  <h2>{ this.state.pokemon.name }</h2>
                </div>
                <div id='pokemon-image-main' className='col-xs-12 col-sm-6 col-md-7 col-lg-8'>
                  <img src={ this.state.pokemon.sprites.front_default } />
                </div>
              </div>
              <div id='pokemon-info' >
                <div id='pokemon-type' className='row'>
                  <label className="col-xs-4 col-sm-4 col-md-3 col-lg-2">Type:</label>
                  { this.getRenderPokemonTypes() }
                </div>
                <div id='pokemon-species' className='row'>
                  <label className="col-xs-4 col-sm-4 col-md-3 col-lg-2">Species:</label>
                  <p className="col-xs-8 col-sm-8 col-md-9 col-lg-10"> { this.state.pokemon.species.name } </p>
                </div>
                <div id='pokemon-height' className='row'>
                  <label className="col-xs-4 col-sm-4 col-md-3 col-lg-2">Height:</label>
                  <p className="col-xs-8 col-sm-8 col-md-9 col-lg-10"> { (this.state.pokemon.height / 10).toString() + "m" } </p>
                </div>
                <div id='pokemon-weight' className='row'>
                  <label className="col-xs-4 col-sm-4 col-md-3 col-lg-2">Weight:</label>
                  <p className="col-xs-8 col-sm-8 col-md-9 col-lg-10"> { (this.state.pokemon.weight / 10).toString() + "kg" } </p>
                </div>
                <div id='pokemon-moves' className='row'>
                  <label className="col-xs-4 col-sm-4 col-md-3 col-lg-2">Moves:</label>
                  { this.getRenderPokemonMoves() }
                </div>
              </div>
              <div id='pokemon-images'>
                <div id='pokemon-images-'>
                </div>
              </div>
            </div>
          }
        </div>
      </div>
    );
  }
}

export default PokemonView;
