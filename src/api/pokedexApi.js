import React from 'react';

class PokedexApi {

  constructor() {
    this.apiBaseUrl = "https://pokeapi.co/api/v2/"; //FAILED DUE TO CORS//"http://pokeapi.salestock.net/api/v2/";

    this.pokemonCount = 964;

    this.pokemon = [];

    this.options = {
      mode: 'no-cors'
    }
  }

  ///
  //
  // Get a basic overview of all pokemon
  //
  ///
  getAllPokemon(offset, limit) {

    //if already got pokemon list, just reuse
    if(this.pokemon.length === this.pokemonCount) {
      return new Promise((resolve, reject) => {
        resolve(this.pokemon);
      });
    }

    //ensure offset and limit are valid
    if(offset === undefined ||
      typeof(offset) != 'number' ||
      offset < 0) {

      offset = 0;
    }
    if(limit === undefined ||
      typeof(limit) != 'number' ||
      limit < 0) {

      limit = this.pokemonCount;
    }

    //prepare path for the api call
    const path = this.apiBaseUrl + "pokemon/?offset=" + offset + "&limit=" + limit;

    //fetch the data
    return new Promise((resolve, reject) =>  {
      fetch(path)
        .then(response => response.json())
        .then(data => {
            if(data.count !== this.pokemonCount) {
              this.pokemonCount = data.count;
            }
            resolve(data);
          }
        )
        .catch(error => reject(error));
    });
  }



  ///
  //
  // Get detailed information for one pokemon, based on it's name
  //
  ///
  getPokemon(name) {

    //ensure the name is valid
    if(name === undefined ||
      typeof(name) !== 'string' ||
      !name) {

        throw( new Error('Invalid name parameter'));

    }

    //prepare the path
    const path = this.apiBaseUrl + "pokemon/" + name;

    //fetch the data
    return new Promise((resolve, reject) => {
      fetch(path)
        .then(response => response.json())
        .then(data => {
          if(data.count !== this.pokemonCount) {
            this.pokemonCount = data.count;
          }
          resolve(data);
        })
        .catch(error => reject(error));
    });
  }

}

const pokedexApi = new PokedexApi();
export default pokedexApi;
