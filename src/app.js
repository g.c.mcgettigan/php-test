import React, {Component}  from 'react';
import { BrowserRouter as Router, Route, Link, Switch, withRouter } from "react-router-dom";
import SearchView from './views/search.js';
import PokemonView from './views/pokemon.js';
import pokedexApi from './api/pokedexApi.js';

class App extends Component {
    constructor(props) {
      super(props);

      this.state = {
        pokemon: [],
        pokemonCount: 0
      }
    }

    componentDidMount() {

      pokedexApi.getAllPokemon()
        .then((result) => {

          var newState = {
            pokemonCount: result.count,
            pokemon: result.results,
            //pageCount: Math.ceil(result.count / this.state.itemsPerPage)
          }

          this.setState(newState);
      });
    }

    render() {
      const noRoute = function() {
        return (<h2>No route found</h2>);
      }

      let app = (
        <div id='app' className='content'>
          <div id='app-header'>
            <div id='app-header-top'>
              <h1 id='app-header-top-title'>
                Pokedex
              </h1>
            </div>
          </div>
          <div id='app-content'>

            <Switch>
              <Route path={["/", "/search"]} exact>
                <SearchView pokemon={this.state.pokemon} pokemonCount={this.state.pokemonCount} />
              </Route>
              <Route path="/pokemon/:name" component={PokemonView}>
              </Route>
              <Route component={noRoute} />
            </Switch>

          </div>
        </div>
      );

      return app;
    }

}

export default withRouter(App);
