<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pokédex</title>

        <!-- React Scripts -->
        <!-- <script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
        <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script> -->
        <!-- JSX support -->
        <!-- <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script> -->

        <!-- <script type="text/babel" src="./src/views/search.js"></script>
        <script type="text/babel" src="./src/views/pokemon.js"></script> -->
        <link
          rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous"
        />
        <link
          rel='stylesheet'
          href="<?=dirname($_SERVER['PHP_SELF'])?>/../src/css/style.css"
          crossorigin="anonymous"
        />
    </head>
    <body>
      <div id="react-root">
      </div>

      <script type="text/javascript" src="<?=dirname($_SERVER['PHP_SELF'])?>/main-bundle.js"></script>
    </body>
</html>
